<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NewsController@index');
Route::get('/noticia/{id}', 'NewsController@show');
Route::get('/editar/{id}', 'NewsController@formCadastro');
Route::get('/cadastrar', 'NewsController@formCadastro');
Route::post('/cadastrar', 'NewsController@store');
Route::post('/atualizar', 'NewsController@update');
Route::get('/pesquisar', 'NewsController@search');

