<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use Illuminate\Validation\Rule;
use Validator;
use DB;
use Dotenv\Validator as DotenvValidator;

class NewsController extends Controller
{


    protected $news;


    public function __construct(News $news)
    {
        $this->news = $news;
    }


    /**
     * Display a view of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $n = $this->news->get();
        return view('listagem',['news'=> $n]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validate = Validator::make($input, $this->news->rules);

        if ($validate->fails()) {
            return ['status' => false, 'msg' => $validate->errors()->all()];
        }

        $msg = '';
        if ($id = $this->news->insertGetId(['titulo' => $input['titulo'],
                                 'conteudo' => $input['conteudo']])) {
            $msg = "Noticia cadastrada com sucesso";
        } else {
            $msg = "Houve um erro ao cadastrar a noticia";
        }

        $n = $this->news->where('id',$id)->get()->toArray()[0];
        $n['msg'] = $msg;

        return view('cadastrar', $n);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();

        $validate = Validator::make($input, $this->news->rules);

        if ($validate->fails()) {
            return ['status' => false, 'msg' => $validate->errors()->all()];
        }
        $id = $input['id'];
        $msg = '';

        if ($this->news->where('id', $id)->update(['titulo' => $input['titulo'], 'conteudo' => $input['conteudo']]) ) {
            $msg = "Noticia atualizada com sucesso";
        } else {
            $msg = "Houve um erro ao atualizar a noticia";
        }

        $n = $this->news->where('id',$id)->get()->toArray()[0];
        $n['msg'] = $msg;

        return view('cadastrar', $n);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //$input = $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $n = $this->news->where('id',$id)->get();
        return view('noticia',['news'=>$n]);
    }

    /**
     * Display the storage form.
     *
     * @return \Illuminate\Http\Response
     */
    public function formCadastro($id=0)
    {
        $n = ['id'=> 0, 'titulo' => '', 'conteudo'=> ''];
        if ($id > 0 ) {
            $n = $this->news->where('id',$id)->get()->toArray()[0];
        }
        $n['msg'] ='';
        return view('cadastrar', $n);
    }


    public function search(Request $request)
    {
        $input = $request->all();
        $titulo = $input['busca_news'];
        $n = $this->news->where('titulo', 'like', $titulo.'%')->get();
        return view('listagem',['news'=> $n]);
    }

}
