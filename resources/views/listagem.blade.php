@extends('app')

@section('content')
    @foreach ($news as $new)
        <div class="box" >
            <a href="/noticia/{{ $new->id }}">
                <p class="titulo">{{ $new->titulo }}</p>
                <p class="conteudo">{{ $new->conteudo }}</p>
            </a>
        </div>
    @endforeach
@endsection