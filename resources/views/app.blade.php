<!DOCTYPE html>
<html class="loading" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    {{--CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .divFlex {
            display: flex;
            justify-content: center;
        }
        /*.divFlex > div {
            margin: 0px;
            padding: 20px;
            font-size: 20px;
        }*/

        body {
            background-color: #EFEFEF;
            margin: 0px;
            text-align: center;
            font-family: 'Open Sans',Arial,sans-serif;
        }
        nav {
            background-color: #7952B3;
            text-align: center;
            position: fixed;
            width: 100%;
        }
        footer {
            background-color: #7952B3;
            position: fixed;
            bottom: 0;
            width: 100%;
            text-align: center;
        }

        .logo {
            width: 80px;
        }

        a {
            text-decoration: none;
            color: #fff;
            font-weight: bold;
            font-family:'Lucida Sans';
            font-size: 16px;
        }
        
        a:hover{
            text-decoration: underline;
        }
        
        .container {
            display: flex;
            align-items: stretch;
            margin: 0px;
            padding: 20px;
            font-size: 20px;
            max-width: 1024px;
            flex-direction: row;
            width: 100%;
        }

        .content {
            display: flex;
            flex-wrap: wrap;
            align-content: space-between;
            text-align: left;
            margin-top: 80px;
        }
        
        .box {
            width: 30%;
            margin: 0px;
            padding: 10px;
            font-size: 20px;
        }
        .row-form {
            width: 100%;
            margin: 0px;
            padding: 10px;
            font-size: 20px;
        }
        .content>a {
            color: #000;
        }
        .box>a{
            color: #000;
        }

        .titulo {
            font-size: 20px;
        }
        .conteudo{
            font-size: 14px;
        }
        label { font-size: 14px;}

    </style>
    </head>
    <body>
        <nav class="divFlex">
            <div class="container" >
                <div style="width: 40%; text-align: left;"><img src="{{ asset('assets/images/logo.png') }}" class="logo"> </div>
                <div style="width: 15%;"> 
                    <a href="/cadastrar">Cadastrar Noticia</a> 
                </div>
                <div style="width: 15%;"> 
                    <a href="/">Exibir Noticias</a>
                </div>
                <div style="width: 30%;"> 
                    <form action="/pesquisar" method="GET">
                    <input type="text" name="busca_news">
                    <input type="submit" value="Buscar"></input>
                    </form>
                </div>
                <div>

                </div>
            </div>
        </nav>
        <main class="divFlex">
            <div class="container content" >
                @yield('content')
            </div>
        </main>
        <footer class="divFlex" style="color: #fff; ">
        <p>Desenvolvido por Izaque Carvalho</p>
        </footer>

    </body>
    <script>

    </script>
</html>