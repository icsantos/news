@extends('app')

@section('content')

        @if ($id > 0)
            <div class="row-form">
                <p>Editar Registro<p>
                <hr>
            </div>
            <form action="/atualizar" method="POST" style="width: 100%;">
            <input type="hidden" name="id" value="{{ $id }}">
        @else
            <div class="row-form">
                <p>Inserir Registro<p>
                <hr>
            </div>
            <form action="/cadastrar" method="POST" style="width: 100%;">
            <input type="hidden" name="id" value="0">
        @endif
        
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        <div class="row-form">
            <label>Titulo </label></br>
            <input type="text" name="titulo" value="{{ $titulo }}" style="width: 500px;">
        </div>
        <div class="row-form">
            <label>Conteudo </label></br>
            <textarea name="conteudo" cols="100" rows="10">{{ $conteudo }}</textarea>
        </div>

        <div class="row-form" >
            <input type="submit" value="Salvar">
        </div>
        <div class="row-form">
            <p>{{ $msg }}<p>
        </div>
        </form>

@endsection